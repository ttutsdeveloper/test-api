// import prizes from '../data/prize.json';
import axios from 'axios';

import { Prize } from '../interfaces/Prize';

const getAllPrizes = (): Promise<Prize[]> => {
    return new Promise(async(resolve, reject) => {
       const result =  await axios.get('https://api.nobelprize.org/v1/prize.json');
        console.log(result.data);
        resolve(result.data['prizes'] as Prize[]);
    });
}

export default {
    getAllPrizes,
}