export interface Prize {
    year: number | string,
    category: string,
    laureates: Laureate[],
    overallMotivation: string
}

export interface Laureate {
    id: number | string,
    firstname: string,
    surname: string,
    motivation: string,
    share: number | string
}