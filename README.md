Development Setup

**Prerequisite**
1. NodeJS
2. Visual Studio Code
3. Postman
4. Git

**To Run the API**
1. Clone the Project
2. Open the terminal and go to the project directory
3. Run `npx.tsc` and `node index.js`
4. Open Postman and create a new POST request using the details below


    URL: localhost:3000/api/prizes

    METHOD: POST

    BODY:  {
            "firstname": "<value>",
            "surname": "<value>",
            "category": "<value>",
            "year": "<value>",
        }
   
6. That's all. Happy Debugging
