import * as http from 'http';
import { Laureate, Prize } from './interfaces/Prize';

export const getPostData = async (req: http.IncomingMessage) => {
    return new Promise((resolve, reject) => {
        try {
            let body = '';
            req.on('data', (chunk) => {
                body += chunk.toString();
            });

            req.on('end', () => {
                resolve(body);
            });
        } catch (error) {
            reject(error);
        }
    });
}

export const filterPrizes = (prizes: Prize[], options?: { firstname?: string; surname?: string; category?: string, year?: number }) => {
    return prizes.filter(prize => 
        (!options?.category || prize.category.toLowerCase() === options.category.toLowerCase()) &&
        (!options?.year || prize.year === options.year) &&
        (!options?.firstname || prize.laureates?.some(laureate => (!options.firstname || laureate.firstname?.toLowerCase() === options.firstname.toLowerCase()))) &&
        (!options?.surname || prize.laureates?.some(laureate => (!options.surname || laureate.surname?.toLowerCase() === options.surname.toLowerCase())))
    );
}
