"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.filterPrizes = exports.getPostData = void 0;
const getPostData = (req) => __awaiter(void 0, void 0, void 0, function* () {
    return new Promise((resolve, reject) => {
        try {
            let body = '';
            req.on('data', (chunk) => {
                body += chunk.toString();
            });
            req.on('end', () => {
                resolve(body);
            });
        }
        catch (error) {
            reject(error);
        }
    });
});
exports.getPostData = getPostData;
const filterPrizes = (prizes, options) => {
    return prizes.filter(prize => {
        var _a, _b;
        return (!(options === null || options === void 0 ? void 0 : options.category) || prize.category.toLowerCase() === options.category.toLowerCase()) &&
            (!(options === null || options === void 0 ? void 0 : options.year) || prize.year === options.year) &&
            (!(options === null || options === void 0 ? void 0 : options.firstname) || ((_a = prize.laureates) === null || _a === void 0 ? void 0 : _a.some(laureate => { var _a; return (!options.firstname || ((_a = laureate.firstname) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === options.firstname.toLowerCase()); }))) &&
            (!(options === null || options === void 0 ? void 0 : options.surname) || ((_b = prize.laureates) === null || _b === void 0 ? void 0 : _b.some(laureate => { var _a; return (!options.surname || ((_a = laureate.surname) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === options.surname.toLowerCase()); })));
    });
};
exports.filterPrizes = filterPrizes;
