import * as http from 'http';
import { getPrizes } from './controllers/prizeController';

const server = http.createServer((req: http.IncomingMessage, res: http.ServerResponse) => {
    if (req.url === '/api/prizes' && req.method === 'POST') {
        getPrizes(req, res)
    } else {
        res.writeHead(404, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({message: 'Route Not Found'}));
    }
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => { console.log(`Server is running on port ${PORT}`)});
