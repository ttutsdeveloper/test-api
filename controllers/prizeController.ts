import * as http from 'http';
import Prize from '../models/prizeModel';
import { filterPrizes, getPostData } from '../utils';

/**
 * Get Prizes
 * 
 * @route  POST /api/getPrizes
 * @param req 
 * @param res 
 * @returns 
 */
export const getPrizes = async (req: http.IncomingMessage, res: http.ServerResponse) => {
    try {
        const prizes = await Prize.getAllPrizes();

        const body = await getPostData(req);

        const { firstname, surname, category, year } = JSON.parse(body as any);

        const filteredPrizes = filterPrizes(prizes,  { firstname, surname, category, year });

        res.writeHead(200, { 'Content-Type': 'application/json' })
        return res.end(JSON.stringify(filteredPrizes))  

    } catch (error) {
        console.log(error);
        res.writeHead(500, { 'Content-Type': 'application/json' })
        return res.end(JSON.stringify({message: 'Missing/Invalid parameters' }))  
    }
}