"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPrizes = void 0;
const prizeModel_1 = __importDefault(require("../models/prizeModel"));
const utils_1 = require("../utils");
/**
 * Get Prizes
 *
 * @route  POST /api/getPrizes
 * @param req
 * @param res
 * @returns
 */
const getPrizes = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const prizes = yield prizeModel_1.default.getAllPrizes();
        const body = yield (0, utils_1.getPostData)(req);
        const { firstname, surname, category, year } = JSON.parse(body);
        const filteredPrizes = (0, utils_1.filterPrizes)(prizes, { firstname, surname, category, year });
        res.writeHead(200, { 'Content-Type': 'application/json' });
        return res.end(JSON.stringify(filteredPrizes));
    }
    catch (error) {
        console.log(error);
        res.writeHead(500, { 'Content-Type': 'application/json' });
        return res.end(JSON.stringify({ message: 'Missing/Invalid parameters' }));
    }
});
exports.getPrizes = getPrizes;
